#!/bin/bash

number="2"

# -eq = equal to
# -ne = not equal
# -it = is less than
# -le = less than
# -gt = grather than
# -ge = grather than or equal to

if [[ 100 -eq 10 ]]; then
	echo "It is true"
else
	echo "It is not true"
fi

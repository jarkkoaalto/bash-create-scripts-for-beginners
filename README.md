# Bash Create Scripts for beginners #

## Introduction ##

Scripting? What can I do?

- Anything you can do in the terminal you can automate with bash.
- Create thousand of files in seconds
- Append data - Append numbers to the beginning of files
- Change name of files or folders
- Delete all files in the computer containnig certain text or name 
- Send Emails

### Tips ###

- Write code
- Ask questions only when you tried figuring it out first
- Treat everyone with respect

### Kernel ###

The kernel is a computer program that is the core of computer's operating system.

It handles basic tasks such as:
- Input and output request from software
- Memory
- Keyboards
- Monitors
- Printers
- Speackers
- Hard Disks

### Shell ###

- A Shell provides an interface between the user and a operation system
- The os starts a shell for each current logged in user.

There are different shells
- Each of them is created in a similar way but with minor differences:  The Bourne sherll the c shell e.g.

### What is BASH ###

- the most popular commandline interpreter
- First UNIX Shell was written by Ken Thompson in 1971
- BASH Stands for Bourne Again Shell - Named After Steven Bourne a computer scientist who developed it while working at AT&T in 1977
- BASH us default shel for MAC OS and Linux Systems


## Section 2: Creating Basic Scripts ##

Create multiple files fast

```
touch example-{1..100}.txt
```
Remove files fast
```
rm example-{1..100}.txt
```

```
echo {A..Z}

echo {A..y}

echo {a..z}

echo {z..a}

{edwin,jose,maria,peter}.txt

```

### Search word in file ###

```
[jarkko@localhost search]$ grep Edwin file100.txt
Ut quis odio et leo tincidunt suscipit vitae ac magna. Aliquam porta ultricies venenatis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Edwin In ultricies consequat faucibus. In non vestibulum dolor. Sed non erat urna. Vestibulum luctus posuere aliquam.
```

or you can search capital or lowercase letters

```
[jarkko@localhost search]$ grep -i e file100.txt
```

## Operators Text Reference ##

### Some String Operators ###

``` 
- = or == is equal to
- != is not equal to
- < is less that in ASCII alphabetical order
- > is greater than in ASCII alphabetical order
- -z test than the string is empty (null)
- -n test that a string is not null 

```

Some operators for dealing with numbers

```
- -eq is equal to 
- -ne is not equal to
- -lt is less than
- -le is less than or equal to
- -gt is grater than
- -ge is greater than or equal to
```

Some more handy operators

```
- -e Does a file exits
- -f test if a file
- -d test if a directory
- -L test if a symbolic link
- -N if a file was modified after it was last read
- -O if the current user owns the file
- -G if the file's group id matches the current user's
- -s test if a file has a size greater than 0
- -r that if the file has read permission
- -w test if the file has writen permission
- -x test if the file has execute permission
```









#!/bin/bash

# if read-permission.txt chmod is 300 a: It is not true
# if read-permission.txt chmod is 777 a: It is true

if [[ -r "read-permission.txt" ]]; then
	echo "It is true"
else 
	echo "It is not true"
fi

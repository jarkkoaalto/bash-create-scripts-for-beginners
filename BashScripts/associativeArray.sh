#!/bin/bash

declare -A theFirstArray
theFirstArray[brand]="BMW"
theFirstArray["wheels"]="4"
theFirstArray["doors"]="2"

echo ${theFirstArray[brand]} and ${theFirstArray["doors"]} doors and ${theFirstArray["wheels"]} wheels.

#!/bin/bash

declare -A theArray
theArray["name"]="John"
theArray["lastname"]="Doe"
theArray["age"]="36"
for index in ${!theArray[@]}
do
echo "$index: ${theArray[$index]}"
done
